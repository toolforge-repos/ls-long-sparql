/*
Link directly to long SPARQL queries hosted on wiki pages
Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use super::rocket;
use rocket::http::Status;
use rocket::local::blocking::Client;

#[test]
fn healthz() {
    let client = Client::tracked(rocket()).unwrap();
    let response = client.get("/healthz").dispatch();
    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.into_string().unwrap(), "OK");
}

#[test]
fn redirect() {
    let client = Client::tracked(rocket()).unwrap();
    let response = client.get("/p/19420423").dispatch();
    assert_eq!(response.status(), Status::Ok);
    let resp = response.into_string().unwrap();
    dbg!(&resp);
    assert!(resp.contains("https://query.wikidata.org/#%23%20Added%202020-07"));
}
