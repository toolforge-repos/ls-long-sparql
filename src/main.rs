/*
Link directly to long SPARQL queries hosted on wiki pages
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#[macro_use]
extern crate rocket;
use anyhow::{anyhow, Result};
use mwapi_responses::prelude::*;
use mwbot::{parsoid::*, Bot};
use rocket::http::ContentType;
use url::Url;

// Placeholder props
#[query(prop = "info", inprop = "associatedpage")]
struct Response;

/// Go from page_id to title
async fn get_title(bot: &Bot, page_id: u32) -> Result<String> {
    let mut params = Response::params().to_vec();
    let page_id = page_id.to_string();
    params.push(("pageids", &page_id));
    let result: Response = bot.get_api().get(&params).await?;
    let page = &result.query.pages[0];
    if page.missing {
        Err(anyhow!("Page doesn't exist"))
    } else {
        Ok(page.title.clone())
    }
}

/// Turn proto-rel links into absolute ones
fn fix_url(input: String) -> String {
    if input.starts_with("//") {
        format!("https:{}", input)
    } else {
        input
    }
}

/// Extract the first query.wikidata.org link out of a page
async fn get_target(bot: &Bot, title: &str) -> Result<String> {
    let page = bot.get_page(title);
    let code = page.get_html().await?;
    for link in code.filter_external_links() {
        let target = fix_url(link.target());
        match Url::parse(&target) {
            Ok(url) => {
                if let Some(host) = url.host_str() {
                    if host == "query.wikidata.org" {
                        return Ok(target);
                    }
                }
            }
            Err(err) => {
                dbg!(err);
            }
        }
    }
    Err(anyhow!("Unable to find a URL to query.wikidata.org"))
}

async fn build_index(page_id: u32) -> Result<String> {
    let bot = Bot::from_default_config().await?;
    let title = get_title(&bot, page_id).await?;
    let url = get_target(&bot, &title).await?;
    println!("URL target is {} long", url.len());
    // Toolforge has multiple layers of nginx proxies which have various limits
    // on the length of HTTP headers. So instead of doing a standard HTTP 302
    // with a "Location:" header, use http-equiv=refresh which should have no
    // length limit. This is bad but fine for a proof-of-concept.
    let html = format!(
        r#"
<html>
    <head>
        <meta http-equiv="refresh" content="0;url={}">
    </head>
</html>
    "#,
        url
    );
    Ok(html)
}

/// Route for index requests
#[get("/p/<page_id>")]
async fn index(page_id: u32) -> Result<(ContentType, String), String> {
    match build_index(page_id).await {
        Ok(redir) => Ok((ContentType::HTML, redir)),
        Err(err) => Err(format!("Error: {}", err)),
    }
}

#[get("/healthz")]
fn healthz() -> &'static str {
    "OK"
}

/// Start Rocket
#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![index, healthz])
}

#[cfg(test)]
mod tests;
